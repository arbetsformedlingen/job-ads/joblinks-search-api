FROM docker.io/library/python:3.11.8-slim-bookworm

EXPOSE 8081

ENV FLASK_APP=joblinks.joblinks_api \
    TZ=Europe/Stockholm \
    PROC_NR=1 \
    HARAKIRI=120

RUN apt-get update -y && \
    apt-get install -y \
    git \
    build-essential && \
    apt-get clean -y && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY . /app

WORKDIR /app

RUN python -m pip install --upgrade pip poetry && python -m poetry config virtualenvs.create false
RUN python -m poetry install
RUN python -m poetry run pytest tests/unit_tests
RUN rm -rf tests && \
    echo "" && echo Application: $FLASK_APP && echo Process total: $PROC_NR && echo ""

CMD  uwsgi --http :8081 --manage-script-name --mount /=${FLASK_APP:-jobsearch}:app --ini uwsgi.ini
