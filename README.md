# JobAd Links API
This repository contains code for JobAd Links APIs.

Requirements
Python 3.10.9+ (but less than 3.11)
Access to a host or server running Elasticsearch
Environment variables
The application is entirely configured using environment variables.

Default values are provided with the listing.

Application configuration
ES_HOST=127.0.0.1
Specifies which elastic search host to use for searching.

ES_PORT=9243
Port number to use for elastic search

ES_INDEX=joblinks
Specifies which index to search ads from (JobLinks)

ES_USER
Sets username for elastic search (no default value)

ES_PWD
Sets password for elastic search (no default value)

Flask
FLASK_APP
The name of the application. Set to "joblinks_api" for JobAd Links.

## Set up repo in IntelliJ or PyCharm:
- Install Python 3.10.9+ if not already done. Keep track of installation dir. https://www.python.org/downloads/
- Install poetry on computer based on this guide: https://www.jetbrains.com/help/idea/poetry.html#baba6780
  - On windows, open power shell:
  - Install: 
    ```
    (Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py
    ```
  - Set env-variable (i.e where windows user is the logged in in user. Don't forget to change value for [windows user] before running):
    ```
    $Env:Path += ";C:\Users\[windows user]\AppData\Roaming\Python\Scripts"; setx PATH "$Env:Path"
    ```
- Create a Poetry environment in IntelliJ or PyCharm:
  - Ctrl+Alt+Shift+S -> SDK:s -> Add Python SDK -> Poetry Environment:
  - Base Interpreter = path to Python 3.10.9+ installation.
  - Poetry Executable = path to Poetry installation, same as env-variable for poetry.
  - Choose Poetry environment in IntelliJ or PyCharm:
    - Ctrl+Alt+Shift+S -> Project -> SDK: Choose your newly created environment, "Poetry (joblinks-search-api)


- Optional (not sure if necessary), install poetry dependencies from terminal:
  - In IntelliJ terminal:
    ```
    python -m pip install --upgrade pip poetry
    ```
    ```
    python -m poetry install
    ```
## Set up run configuration in IntelliJ or Pycharm:
- Go to menu "Run" and choose "Edit configurations"
- Add new config -> Python
  - Script Path:[path to repo]\joblinks\joblinks_api.py
  - Env. variables: FLASK_ENV=development;ES_HOST=;ES_INDEX=;ES_PORT=;ES_PWD=;ES_USER=
  - Use specified interpreter: [the poetry environment]
  - Working directory:[path to repo]\joblinks-search-api\joblinks
  - Check "Add content roots to PYTHONPATH" + Check "Add source roots to PYTHONPATH"

## Docker

### Build
docker build -t joblinkssearchapi:latest .

### Run
docker run -d --env-file=docker.env -p 8081:8081 --name joblinkssearchapi joblinkssearchapi

Swagger is available on `http://localhost:8081/`

### Check log file
docker logs joblinkssearchapi --details -f

### Stop & remove image
docker stop joblinkssearchapi && docker rm joblinkssearchapi || true

### Debug Docker
docker logs joblinkssearchapi --details -f
docker exec -t -i joblinkssearchapi /bin/bash
docker run -it --rm joblinkssearchapi:latest

  
## Installation and running (in general):
To start up the application, set the appropriate environment variables as described above. Then run the following commands.

$ pip install poetry
$ poetry install
$ export FLASK_APP=joblinks/joblinks_api.py
$ export FLASK_ENV=development
$ flask run
Go to http://127.0.0.1:5000 to access the swagger API

Tests
Run unit test
$ pytest tests/unit_tests
The environment variable `DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP` is set to True in `tests\unit_tests\conftest.py`, otherwise code that requires database connection runs

Run integration tests
When running integration tests, an actual application is started, so you need to specify environment variables for elastic search in order for it to run properly.

$ pytest tests/integration_tests


Run api test
Requires Flask to be running and ads from 2022-03-08 to be loaded into Elastic, see earlier step
$ pytest tests/api_tests
