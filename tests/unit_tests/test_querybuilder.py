from datetime import datetime, timedelta
from joblinks.repository.querybuilder import QueryBuilder
from joblinks.repository.querybuilder_freetext import QueryBuilderFreetext
from joblinks.repository.querybuilder_location import QueryBuilderLocation
from tests.unit_tests.test_resources import mock_for_querybuilder_tests as mock
import pytest




@pytest.mark.unit
@pytest.mark.parametrize("querystring, expected", [('xx,', 'xx '),  # trailing space
                                                   ('xx.', 'xx'),
                                                   ('xx!', 'xx'),
                                                   ('xx?', 'xx'),
                                                   ('xx:', 'xx'),
                                                   ('xx;', 'xx'),
                                                   ('.xx', 'xx'),
                                                   (',xx', ' xx'),  # leading space
                                                   ('!xx', 'xx'),
                                                   ('?xx', 'xx'),
                                                   (':xx', 'xx'),
                                                   (';xx', 'xx'),
                                                   (';xx', 'xx'),
                                                   (' xx', ' xx'),
                                                   ('x x', 'x x'),
                                                   ('x,x ', 'x x '),  # trailing space
                                                   ('x.x ', 'x.x '),
                                                   ('x!x ', 'x!x '),
                                                   ('x?x ', 'x?x '),
                                                   ('x:x ', 'x:x '),
                                                   ('x;x ', 'x;x '),
                                                   ('xx ', 'xx '),
                                                   ('x/y', 'x/y'),
                                                   ('.x/y', 'x/y'),
                                                   ('x/y.', 'x/y'),
                                                   ('x / y.', 'x / y'),
                                                   ('y,.!?:; x', 'y  x'),
                                                   ('x,y.z!1?2:3;4 x', 'x y.z!1?2:3;4 x'),
                                                   ('12345x', '12345x'),
                                                   ('.12345', '12345'),
                                                   ('.12345.', '12345'),
                                                   ('.12345.', '12345'),
                                                   (',12345', ' 12345'),
                                                   (',12345,', ' 12345 '),
                                                   ('\\x', '\\x'),
                                                   ('\\x,', '\\x '),
                                                   ('\\x.', '\\x'),
                                                   ('\\.x.', '\\.x'),
                                                   ('.\\.x.', '\\.x'),
                                                   (',\\.x.', ' \\.x'),
                                                   ])
def test_querystring_char_removal(querybuilder_freetext, querystring, expected):
    formatted = querybuilder_freetext._remove_unwanted_chars_from_querystring(querystring)
    #formatted = querybuilder._remove_unwanted_chars_from_querystring(querystring)
    assert formatted == expected


@pytest.mark.unit
@pytest.mark.parametrize("word, expected", [("[python3]", "\\[python3\\]"),
                                            ("python3", "python3"),
                                            ("asp.net", "asp\\.net"),
                                            ("c++", "c\\+\\+"),
                                            ("word+word", "word\\+word"),
                                            (".net", "\\.net"),
                                            (None, ''),
                                            ("|word|", "\\|word\\|"),
                                            ("<word>", "\\<word\\>"),
                                            ("word?", "word\\?"),
                                            (r"\word", "\\\\word"),
                                            ("*word", "\\*word"),
                                            ("&word", "\\&word"),
                                            ("^word", "\\^word"),
                                            ("$word", "\\$word"),
                                            ('"word"', '\\"word\\"'),
                                            ("\\word", "\\\\word"),
                                            (r"\word", "\\\\word"),
                                            ("[',word ']", "\\[\\',word \\'\\]"),
                                            ("{',word '}", "\\{\\',word \\'\\}"),
                                            ("(',word ')", "\\(\\',word \\'\\)"),
                                            # no rewrite
                                            ("%word", "%word"),
                                            ("word#", "word#"),
                                            ("/word", "/word"),
                                            ("§word", "§word"),
                                            ])
def test_rewrite_word_for_regex(querybuilder, word, expected):
    assert querybuilder._rewrite_word_for_regex(word) == expected


@pytest.mark.unit
def test_rewrite_querystring(querybuilder):
    # concepts blob should be handled differently
    concepts = {'skill': [
        {'term': 'c++', 'uuid': '1eb1dbeb-e22a-53cb-bb28-c9fbca5ad307',
         'concept': 'C++', 'type': 'KOMPETENS',
         'term_uuid': '9734cba6-eff8-5cdc-9881-392a4345e57e',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'c#', 'uuid': 'af98ee4d-49e7-5274-bc76-a9f119c1514c',
         'concept': 'C-sharp', 'type': 'KOMPETENS',
         'term_uuid': '37da571a-a958-5b3d-a857-0a0a6bbc88cf',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'asp.net', 'uuid': '18d88a83-55d5-527b-a800-3695ed035a0c',
         'concept': 'Asp.net', 'type': 'KOMPETENS',
         'term_uuid': '280d3fa7-becd-510d-94ac-c67edb0ef4e0',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'c++', 'uuid': '1eb1dbeb-e22a-53cb-bb28-c9fbca5ad307',
         'concept': 'C++', 'type': 'KOMPETENS',
         'term_uuid': '9734cba6-eff8-5cdc-9881-392a4345e57e',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'tcp/ip', 'uuid': '09df5ef2-357f-5cfc-9333-dec2e220638a',
         'concept': 'Tcp/ip', 'type': 'KOMPETENS',
         'term_uuid': 'a18b2945-779f-5032-bbaa-c7945a63055f',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''}], 'occupation': [
        {'term': 'specialpedagog',
         'uuid': '4872acf8-ea61-50fe-8a7e-7af82b37ce9e',
         'concept': 'Specialpedagog',
         'type': 'YRKE', 'term_uuid': 'c6db8f6e-69f7-5aae-af18-2a1eae084eba',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'lärare', 'uuid': 'eadc9f5f-35c0-5324-b215-ea388ca054ff',
         'concept': 'Lärare', 'type': 'YRKE',
         'term_uuid': '300844f7-77b6-539e-a8d7-1955ce18a00c',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'speciallärare',
         'uuid': '2708c006-d8d0-5920-b434-a5968aa088e3',
         'concept': 'Speciallärare',
         'type': 'YRKE', 'term_uuid': 'cd50806f-3c52-5e73-a06e-c7a65f7410a4',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''}], 'trait': [],
        'location': [], 'skill_must': [],
        'occupation_must': [], 'trait_must': [], 'location_must': [],
        'skill_must_not': [],
        'occupation_must_not': [], 'trait_must_not': [],
        'location_must_not': []}
    assert querybuilder._rewrite_querystring("specialpedagog lärare speciallärare", concepts) == ""
    assert querybuilder._rewrite_querystring("specialpedagog speciallärare lärare", concepts) == ""
    assert querybuilder._rewrite_querystring("lärare speciallärare flärgare", concepts) == "flärgare"
    assert querybuilder._rewrite_querystring("korvprånglare c++ asp.net [python3] flärgare",
                                             concepts) == "korvprånglare [python3] flärgare"
    assert querybuilder._rewrite_querystring("tcp/ip", concepts) == ""


@pytest.mark.unit
@pytest.mark.parametrize("querystring, expected_phrase, expected_returned_query, test_id", [
    # With these quotes, the query will be returned with some quote modification
    # the 'matches' field will be empty
    ("'gymnasielärare'", [], "'gymnasielärare'", 'a'),
    ("""gymnasielärare""", [], 'gymnasielärare', 'b'),
    ('''gymnasielärare''', [], 'gymnasielärare', 'c'),
    ("gymnasielärare\"", [], 'gymnasielärare""', 'd'),
    ("gymnasielärare\'", [], "gymnasielärare'", 'e'),
    ("\'gymnasielärare", [], "'gymnasielärare", 'f'),
    (r"""gymnasielärare""", [], 'gymnasielärare', 'g'),
    (r'''gymnasielärare''', [], 'gymnasielärare', 'h'),
    ("gymnasielärare lärare", [], 'gymnasielärare lärare', 'i'),
    ("""'gymnasielärare'""", [], "'gymnasielärare'", 'j'),

    # with these quotes, the 'phrases' field has data quoted with single quotes
    # and the query is not returned
    ('''"gymnasielärare" "lärare"''', ['gymnasielärare', 'lärare'], '', 'aa'),
    ('''"gymnasielärare lärare"''', ['gymnasielärare lärare'], '', 'ab'),
    ('"gymnasielärare"', ['gymnasielärare'], '', 'ac'),
    ("\"gymnasielärare\"", ['gymnasielärare'], '', 'ad'),
    ("\"gymnasielärare", ['gymnasielärare'], '', 'ae'),
    ("\"gymnasielärare", ['gymnasielärare'], '', 'af'),
    ('''"gymnasielärare"''', ['gymnasielärare'], '', 'ag'),

    # "normal" quotes, 'phrases' field empty, query returned
    ("gymnasielärare", [], 'gymnasielärare', 'x'),
    ('gymnasielärare', [], 'gymnasielärare', 'y'),
    ('python', [], 'python', 'z'),
])
def test_extract_querystring_different_quotes(querybuilder_freetext, querystring, expected_phrase, expected_returned_query,
                                              test_id):
    """
        Test behavior of querybuilder.extract_quoted_phrases
        when sending strings with different types of quotes
    """
    actual_result = querybuilder_freetext.extract_quoted_phrases(querystring)
    # no plus or minus used in this test, so these fields must be empty
    assert actual_result[0]['phrases_must'] == []
    assert actual_result[0]['phrases_must_not'] == []

    actual_phrases = actual_result[0]['phrases']
    assert actual_phrases == expected_phrase, f"got {actual_phrases} but expected {expected_phrase}"

    actual_returned_query = actual_result[1]
    assert actual_returned_query == expected_returned_query, f"got {actual_returned_query} but expected {expected_returned_query}"


@pytest.mark.unit
@pytest.mark.parametrize("querystring, expected", [
    ("python \"grym kodare\"",
     ({"phrases": ["grym kodare"], "phrases_must": [], "phrases_must_not": []}, "python")),
    ("java \"malmö stad\"",
     ({"phrases": ["malmö stad"], "phrases_must": [], "phrases_must_not": []}, "java")),
    ("python -\"grym kodare\" +\"i am lazy\"",
     ({"phrases": [], "phrases_must": ["i am lazy"], "phrases_must_not": ["grym kodare"]}, "python")),
    ("\"python på riktigt\" -\"grym kodare\" +\"i am lazy\"",
     (
             {"phrases": ["python på riktigt"], "phrases_must": ["i am lazy"],
              "phrases_must_not": ["grym kodare"]},
             "")),
])
def test_extract_querystring_phrases(querybuilder_freetext, querystring, expected):
    assert expected == querybuilder_freetext.extract_quoted_phrases(querystring)


@pytest.mark.unit
@pytest.mark.parametrize("querystring, expected", [
    ("\"i am lazy", ({"phrases": ["i am lazy"], "phrases_must": [], "phrases_must_not": []}, "")),
    ("python \"grym kodare\" \"i am lazy java",
     (
             {"phrases": ["grym kodare", "i am lazy java"], "phrases_must": [], "phrases_must_not": []},
             "python")),
    ("python \"grym kodare\" +\"i am lazy",
     ({"phrases": ["grym kodare"], "phrases_must": ["i am lazy"], "phrases_must_not": []}, "python")),
    ("python \"grym kodare\" -\"i am lazy",
     ({"phrases": ["grym kodare"], "phrases_must": [], "phrases_must_not": ["i am lazy"]}, "python")),
])
def test_extract_querystring_phrases_with_unbalanced_quotes(querybuilder_freetext, querystring, expected):
    assert expected == querybuilder_freetext.extract_quoted_phrases(querystring)


def _assert_json_structure(result, expected):
    return _walk_dictionary(result, expected)


def _walk_dictionary(result, expected):
    if isinstance(result, str) and isinstance(expected, str):
        return result == expected
    else:
        for item in expected:
            if item in result:
                if isinstance(result[item], list):
                    for listitem in result[item]:
                        if _walk_dictionary(listitem, expected[item]):
                            return True
                else:
                    return _walk_dictionary(result[item], expected[item])

        return False


@pytest.mark.unit
def test_bootstrap_query(querybuilder):
    args = {'occupation-group': None,
            'occupation-field': None,
            'municipality': None,
            'region': None,
            'country': None,
            'q': 'sjuksköterska'}
    expected = {'from': 0, 'size': 10, 'query': {'bool': {'must': []}},
                'sort': ['_score', {'date_to_display_as_publish_date': 'desc'}],
                'track_scores': True, 'track_total_hits': True}
    actual = querybuilder._bootstrap_query(args)
    assert expected == actual

@pytest.mark.unit
def test_bootstrap_query_sorted(querybuilder):
    args = {'occupation-group': None,
            'occupation-field': None,
            'municipality': None,
            'region': None,
            'country': None,
            'sort': 'pubdate-asc',
            'q': 'sjuksköterska'}
    expected = {'from': 0, 'size': 10, 'query': {'bool': {'must': []}},
                'sort': [{'date_to_display_as_publish_date': 'asc'}, '_score', {'id': 'asc'}],
                'track_scores': True, 'track_total_hits': True}
    actual = querybuilder._bootstrap_query(args)
    assert expected == actual





@pytest.mark.unit
def test_assemble_queries(querybuilder):
    in_query_dsl = {'from': 0, 'size': 1, 'query': {'bool': {'must': []}}}
    additional_queries = [{'bool': {'must': [{'bool': {
        'should': [{'term': {'keywords.extracted.occupation': {'value': 'sjuksköterska', 'boost': 10}}},
                   {'term': {'keywords.enriched.occupation': {'value': 'sjuksköterska', 'boost': 9}}}, {'match': {
                'originalJobPosting.title.words': {'query': 'sjuksköterska', 'operator': 'and', 'boost': 5}}}]}}]}},
        None, {}]

    expected = {'from': 0, 'size': 1, 'query': {'bool': {'must': [{'bool': {'must': [{'bool': {
        'should': [{'term': {'keywords.extracted.occupation': {'value': 'sjuksköterska', 'boost': 10}}},
                   {'term': {'keywords.enriched.occupation': {'value': 'sjuksköterska', 'boost': 9}}}, {'match': {
                'originalJobPosting.title.words': {'query': 'sjuksköterska', 'operator': 'and', 'boost': 5}}}]}}]}}]}}}

    result = querybuilder._assemble_queries(in_query_dsl, additional_queries)
    assert result == expected


@pytest.mark.unit
def test_ft_query(querybuilder_freetext):
    querystring = 'sjuksköterska'
    queryfields = 'None'
    expected_ft_query = {'bool': {'must': [{'bool': {'should': [{'multi_match': {'query': 'sjuksköterska',
                                                                                 'type': 'cross_fields',
                                                                                 'operator': 'or', 'fields': [
            'originalJobPosting.title^3', 'keywords.extracted.location^5']}}, {'match': {
        'originalJobPosting.title.words': {'query': 'sjuksköterska', 'operator': 'and', 'boost': 5}}}]}}]}}

    result = querybuilder_freetext.build_freetext_query(querystring, queryfields)
    assert result == expected_ft_query



#######################################################################
# Freetext_query
#
# -> querybuilder
# 
# @pytest.mark.unit
# def test_freetext_query(querybuilder):
#     querystring = '+sjuksköterska'
#     queryfields = 'None'
#     expected_ft_query = {'bool': {'must': [{'bool': {'should': [{'multi_match': {'query': 'sjuksköterska',
#                                                                                  'type': 'cross_fields',
#                                                                                  'operator': 'or', 'fields': [
#             'originalJobPosting.title^3', 'keywords.extracted.location^5']}}, {'match': {
#         'originalJobPosting.title.words': {'query': 'sjuksköterska', 'operator': 'and', 'boost': 5}}}]}}]}}

#     result = querybuilder._build_freetext_query(querystring, queryfields)
#     assert result == expected_ft_query



@pytest.mark.unit
def test_ft_query_none(querybuilder_freetext):
    result = querybuilder_freetext.build_freetext_query(None, None)
    assert result is None


@pytest.mark.unit
def test_parse_args(querybuilder):
    args = {'occupation-group': None, 'occupation-field': None, 'municipality': None, 'region': None, 'country': None,
            'sort': None, 'q': 'lärare', 'offset': 0, 'limit': 1}
    expected_query_dsl = {'from': 0, 'size': 1, 'query': {'bool': {'must': [{'bool': {'must': [{'bool': {'should': [{
        'multi_match': {
            'query': 'lärare',
            'type': 'cross_fields',
            'operator': 'or',
            'fields': [
                'originalJobPosting.title^3',
                'keywords.extracted.location^5']}},
        {
            'match': {
                'originalJobPosting.title.words': {
                    'query': 'lärare',
                    'operator': 'and',
                    'boost': 5}}}]}}]}}]}},
                          'sort': ['_score', {'date_to_display_as_publish_date': 'desc'}],
                          'track_scores': True,
                          'track_total_hits': True}
    result = querybuilder.parse_args(args)
    assert result == expected_query_dsl


@pytest.mark.unit
@pytest.mark.parametrize("from_datestring", ['2018-11-16T09:41:53',
                                             datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
                                             ])
def test_filter_timeframe_basic(querybuilder, from_datestring):
    expected_range_query = {'range': {'date_to_display_as_publish_date': {'gte': f"{from_datestring}"}}}
    result = querybuilder._filter_timeframe(from_datestring)
    assert result == expected_range_query


@pytest.mark.unit
@pytest.mark.parametrize("from_datestring", ['0', '8'])
def test_filter_timeframe_complex(querybuilder, from_datestring):
    # TODO: test other combinations that matches re.match(r'^\d+$'

    calculated_time = datetime.now() - timedelta(minutes=int(from_datestring))

    compare_time = calculated_time.strftime("%Y-%m-%dT")  # remove hh, mm, ss. Might still fail at midnight
    expected_range_query_pattern = "{'range': {'date_to_display_as_publish_date': {'gte': "

    result = querybuilder._filter_timeframe(from_datestring)
    assert expected_range_query_pattern in str(result)
    assert compare_time in str(result)


@pytest.mark.unit
def test_filter_timeframe_none(querybuilder):
    assert querybuilder._filter_timeframe(None) is None


@pytest.mark.parametrize('excluded_source, expected_sub_query', [
    ('arbetsformedlingen.se', {'bool': {'must_not': {'nested': {'path': 'source_links',
                                                                'query': {'bool': {'must': [{'term': {
                                                                    'source_links.label': 'arbetsformedlingen.se'}}]}}}}}}),
    ('', None)])
@pytest.mark.unit
def test__build_negative_source_filter_query(querybuilder, excluded_source, expected_sub_query):
    assert querybuilder._build_negative_source_filter_query(excluded_source) == expected_sub_query


@pytest.fixture()
def querybuilder():
    return QueryBuilder(mock.MockTextToConcept())


@pytest.fixture()
def querybuilder_freetext():
    return QueryBuilderFreetext(mock.MockTextToConcept())


@pytest.fixture()
def querybuilder_location():
    return QueryBuilderLocation(mock.MockTextToConcept())


