import pytest
import requests
from tests.test_resources.helper import get_search, random_ad_ids, get_by_id, get_by_id_expect_status_code, \
    check_ad_structure


@pytest.mark.smoke
def test_get_by_id( get_10_random_ads):
    """
    collects 100 ads, pick a subset (1-100) of those ads by random
    and do a new call to the /ad/<id> endpoint with the id of each of the ads
    """
    json_response = get_search(params={'limit': '100'})
    random_ids = random_ad_ids(json_response['hits'])
    for ad_id in random_ids:
        ad = get_by_id(ad_id)
        check_ad_structure(ad)
        assert ad['id'] == ad_id


@pytest.mark.parametrize("ad_id, expected_status_code", [("does not exist", requests.codes.not_found),
                                                         ("this_id_does_not_exist_either", requests.codes.not_found),
                                                         ('x' * 32000, requests.codes.not_found),
                                                         ('0' * 32000, requests.codes.not_found),
                                                         (None, requests.codes.not_found),
                                                         (0, requests.codes.not_found),
                                                         (1, requests.codes.not_found),
                                                         (-1, requests.codes.not_found),
                                                         ])
def test_by_id_negative( ad_id, expected_status_code):
    """
    test with different kinds of non-existent ad ids and check that the http status code is as expected
    """
    get_by_id_expect_status_code(ad_id, expected_status_code),
