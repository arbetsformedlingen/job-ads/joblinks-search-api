import random
import pytest
import requests
from tests.test_resources import test_settings
from joblinks import settings
import tests.test_resources.helper as helper


@pytest.fixture(scope="session")
def session(request):
    """
    creates a Session object which will persist over the entire test run ("session").
    http connections will be reused (higher performance, less resource usage)
    Returns a Session object
    """
    s = requests.sessions.Session()
    s.headers.update(test_settings.headers)
    return s


@pytest.fixture(scope="session")
def total_number_of_ads():
    """
    returns the number of ads in the current dataset
    """
    return helper.get_total({})


@pytest.fixture()
def get_10_random_ads():
    """
    gets a block of 100 ads with random starting point from 1 to MAX_OFFSET (defined in settings)
    picks 10 out of the 100 ads at random
    """
    hits = helper.get_search({'limit': '100', 'offset': random.randint(1, settings.MAX_OFFSET)})['hits']
    assert len(hits) == 100
    random_hits = random.sample(hits, 10)
    return random_hits
