from tests.test_resources.helper import check_ad_structure


def test_workplace_addresses( get_10_random_ads):
    hits = get_10_random_ads

    for hit in hits:
        check_ad_structure(hit)
