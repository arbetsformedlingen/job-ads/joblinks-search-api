import pytest

from tests.test_resources.helper import get_search, check_ad_structure

@pytest.mark.parametrize("query, employer, expected", [
    ("sjuksköterska", 'REGION JÖNKÖPINGS LÄN', 28),
    ("sjuksköterska skåne", 'REGION SKÅNE', 18),
    ("lärare stockholm", 'Stockholms stad', 9),
    ("stockholm", 'Randstad AB', 6),
])
def test_employer( query, employer, expected):
    count = 0
    param = {'q': query, 'limit': 100}
    json_response = get_search(param)
    hits = json_response['hits']
    for hit in hits:
        check_ad_structure(hit)
        if hit['employer']['name'].lower() == employer.lower():
            count += 1
    assert count == expected
