import pytest
import requests

from tests.test_resources.helper import get_search


@pytest.mark.parametrize("limit", [0, 1, 18, 19, 35, 36, 100])
def test_limit_with_query( total_number_of_ads, limit):
    json_response = get_search({'q': 'Sjuksköterska', 'limit': limit})
    assert len(json_response['hits']) == limit


@pytest.mark.parametrize("limit, expected_number_of_hits", [(0, 0), (-0, 0), (1, 1), (99, 99), (100, 100)])
def test_limit_no_query( total_number_of_ads, limit, expected_number_of_hits):
    json_response = get_search(params={'limit': limit})
    assert json_response['total']['value'] == total_number_of_ads
    assert len(json_response['hits']) == expected_number_of_hits


@pytest.mark.parametrize("wrong_limit", [101, -1, -1024, 1024])
def test_limit_wrong( wrong_limit):
    with pytest.raises(requests.exceptions.HTTPError):
        get_search(params={'limit': wrong_limit})
