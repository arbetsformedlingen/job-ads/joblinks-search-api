import pytest
from tests.test_resources.helper import check_number_of_ads, get_search


@pytest.mark.smoke
@pytest.mark.parametrize("query, expected", [
    ('barnmorska', 337),
    ('tandsköterska', 164),
    ('kock', 1468),
    ('Sjuksköterskor', 6155),
    ('Sjuksköterska', 6167),
    ('Stockholm', 11159),
    ('Göteborg', 5088),
    ('Malmö', 3684),
    ('utvecklare', 3240),
    ('barnmorska tandsköterska', 501),
    ('tandsköterska barnmorska', 501),
    ('barnmorska fysioterapeut', 827),  # no free text search
    ('chefsbarnmorska fysioterapeut', 491),  # both are in enrichment - occupation
    ('chefsbarnmorska barnmorska', 338),
    ('developer engineer', 226),
    ("python", 1606),
    ("java", 1495),
    ("java -javautvecklare", 1259),
    ("java python", 2692),
    ("java +python", 1602),
    ("java -python", 1104),
    ("kock -bagare", 1460),
    ("pizzabagare", 212),
    ("bartender", 208),
    ("målare", 128),
    ("målare stockholm", 11),
    ("målare +stockholm", 11),
    ("målare -stockholm", 117),
    ("personlig assistent", 2602),
    ("personlig assistent +göteborg", 42),
    ("personlig assistent -göteborg", 2557),
    ("utvecklare", 3240),
    ("förskollärare", 927),
    ("sjuksköterska", 6167),
    ("sjuksköterska -stockholm", 5985),
    ('lärare kock', 4144),
    ('Sollentuna', 317),
    ('Sollentuna Umeå', 1386),
    ('Sjuksköterska Göteborg', 93),
    ('Helsingborg', 1233),
    ('sjuksköterskor', 6155),
    ('sjuksköterska', 6167),
    ('Uppsala', 1657),
    ('sjuksköterskor Uppsala', 93),
    ("C#", 1180),
    ("c-körkort", 504),
    (".net", 401),
    ("ci/cd", 651),
    ("erp-system", 172),
    ("tcp/ip", 111),
    ("cad-verktyg", 44),
    ("backend-utvecklare", 141),
    ("it-tekniker", 338),

])
def test_freetext_query( query, expected):
    json_response = get_search({'q': query})
    check_number_of_ads(json_response, expected)
    """
    One search term: freetext search is performed
    Multiple search terms:
    Only those where the occupation is found in enrichment are returned. No free text search is done
    """


@pytest.mark.smoke
def test_no_freetext_query_total_value( total_number_of_ads):
    check_number_of_ads(get_search({}), total_number_of_ads)
