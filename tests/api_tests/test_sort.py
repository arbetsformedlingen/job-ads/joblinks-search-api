import requests
from tests.test_resources.helper import list_of_ids_and_date, get_search, get_joblinks_raw


def test_sort_relevance():
    """
    Check that 'sort': 'relevance' (default behaviour) gives the same result as when not using sort param at all
    """
    q = 'distributör'
    params_default = {'q': q, 'limit': 100}
    ids_default = list_of_ids_and_date(get_search(params_default))

    params_relevance = {'q': q, 'limit': 100, 'sort': 'relevance'}
    ids_relevance = list_of_ids_and_date(get_search(params_relevance))

    assert ids_default == ids_relevance


def test_sort_asc_desc():
    """
    Check that sorting ascending gives the same hits but in reverse order as
    when sorting descending
    """
    q = 'brandman'
    params_asc = {'q': q, 'limit': 100, 'sort': 'pubdate-asc'}
    result = get_search(params_asc)
    asc_ids_dates = list_of_ids_and_date(result)

    params_desc = {'q': q, 'limit': 100, 'sort': 'pubdate-desc'}
    desc_ids_dates = list_of_ids_and_date(get_search(params_desc))

    assert len(asc_ids_dates) == len(desc_ids_dates)
    assert asc_ids_dates != desc_ids_dates

    for tpl in asc_ids_dates:
        assert tpl in desc_ids_dates

    # make list with dates for comparing sorting
    asc_dates = []
    desc_dates = []
    for tpl in asc_ids_dates:
        asc_dates.append(tpl[1])
    for tpl in desc_ids_dates:
        desc_dates.append(tpl[1])
    assert asc_dates == list(reversed(desc_dates))


def test_wrong_sort_param():
    response = get_joblinks_raw(params={'sort': 'id'})
    assert response.status_code == requests.codes.BAD_REQUEST
    assert "The value \'id\' is not a valid choice for \'sort\'." in response.text
