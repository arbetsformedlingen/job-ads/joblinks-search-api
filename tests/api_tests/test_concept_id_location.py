import pytest

from tests.test_resources.helper import get_search, get_total, check_number_of_ads, check_geo_concept_ids, negative, check_ad_structure
from tests.test_resources.concept_ids import concept_ids_geo as geo


@pytest.mark.parametrize("city, region, country, expected", [
    (geo.stockholm, geo.stockholms_lan, geo.sverige, 13030),
    (geo.malmo, geo.skane_lan, geo.sverige, 3729),
    (geo.goteborg, geo.vastra_gotalands_lan, geo.sverige, 5488),
    (geo.umea, geo.vasterbottens_lan, geo.sverige, 1085)
])
def test_search_municipality( city, region, country, expected):
    """
    Check that parent concept ids (region, country) are correct when searching for a municipality
    """
    json_response = get_search(params={'municipality': city, 'limit': '100'})
    check_number_of_ads(json_response, expected)
    for hit in json_response['hits']:
        wp_addresses = hit['workplace_addresses']
        check_geo_concept_ids(wp_addresses, [city], region, country)
        check_ad_structure(hit)


@pytest.mark.parametrize("region, expected_cities, country, expected_hits", [
    (geo.vasterbottens_lan, geo.cities_in_vasterbottens_lan, geo.sverige, 2034),
    (geo.skane_lan, geo.cities_in_skane_lan, geo.sverige, 9347),
    (geo.stockholms_lan, geo.cities_in_stockholms_lan, geo.sverige, 18473)
])
def test_search_region( region, expected_cities, country, expected_hits):
    """
    Check that parent (country) and child (municipality) concept ids are correct when searching for ads in a region
    """

    json_response = get_search({'region': region, 'limit': 100})
    check_number_of_ads(json_response, expected_hits)
    for hit in json_response['hits']:
        wp_addresses = hit['workplace_addresses']
        check_geo_concept_ids(wp_addresses, expected_cities, region, country)
        from tests.test_resources.helper import check_ad_structure
        check_ad_structure(hit)

@pytest.mark.parametrize("country, expected",
                         [(geo.sverige, 63152),
                          (geo.norge, 174),
                          (geo.grekland, 3),
                          (geo.schweiz, 3),
                          (geo.estland, 0),
                          (geo.stockholm, 0)  # not a country, expect 0 hits
                          ])
def test_search_country( country, expected):
    """
    Check that correct number of hits is returned when searching with the 'country' parameter and concept_id
    """
    params = {'country': country}
    check_number_of_ads(get_search(params), expected)


@pytest.mark.parametrize("params, expected", [
    ({}, 2831),
    ({'region': geo.stockholms_lan}, 267),
    ({'region': geo.skane_lan}, 185),
    ({'region': [geo.stockholms_lan, geo.skane_lan]}, 267 + 185),
    ({'municipality': geo.stockholm}, 111),
    ({'region': geo.skane_lan, 'municipality': geo.stockholm}, 111 + 185),
    ({'region': negative(geo.skane_lan)}, 1275),

])
def test_locations_with_query( params, expected):
    params.update({'q': 'lärare'})
    check_number_of_ads(get_search(params), expected)


@pytest.mark.parametrize("params, expected", [
    ({'country': geo.norge, 'region': geo.blekinge_lan}, 857),
    ({'country': geo.norge, 'region': geo.skane_lan}, 9513),
    ({'country': geo.norge, 'region': geo.goteborg}, 174),
    ({'country': [geo.sverige, geo.norge], 'region': geo.skane_lan}, 63236),
    ({'country': [geo.schweiz, geo.norge]}, 177),
    ({'country': negative(geo.norge)}, 63442),
    ({'country': negative(geo.sverige)}, 1109),
])
def test_locations( params, expected):
    check_number_of_ads(get_search(params), expected)


def test_negative_country( total_number_of_ads):
    hits_sweden = get_total({'country': geo.sverige})
    hits_not_sweden = get_total({'country': negative(geo.sverige)})
    assert hits_not_sweden > 0
    assert hits_sweden > (total_number_of_ads * 0.5)  # more than 90% of ads should have Sweden as country
