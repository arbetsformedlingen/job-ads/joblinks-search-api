import pytest
from tests.test_resources.helper import get_search, check_number_of_ads
from joblinks.settings import PUBLISHED_AFTER

timestamps_with_expected = [
    ('2020-03-17T15:40:21', 2831),
    ('2021-03-12T15:40:25', 2829),
    ('2021-11-27T15:40:25', 2815),
    ('2022-02-05T15:40:25', 2634),
    ('2022-02-25T15:40:25', 1270),
    ('2022-03-04T15:40:25', 192),
    ('2022-03-05T15:40:25', 166),
    ('2022-03-07T15:40:25', 22),
]


def test_published_after():
    results = []
    for timestamp, exp in timestamps_with_expected:
        params = {'q': 'lärare', PUBLISHED_AFTER: timestamp, 'limit': 0}
        response = get_search(params)
        results.append(response['total']['value'])
    assert results[0] > 0
    # search is done with oldest dates first, so the values are added descending to results list
    # sorting in reverse order (largest first) should not change order
    assert sorted(results, reverse=True) == results
    assert sorted(results, reverse=False) != results


@pytest.mark.parametrize('timestamp, expected', timestamps_with_expected)
def test_date_expected( timestamp, expected):
    params = {'q': 'lärare', PUBLISHED_AFTER: timestamp, 'limit': 0}
    check_number_of_ads(get_search(params), expected)
