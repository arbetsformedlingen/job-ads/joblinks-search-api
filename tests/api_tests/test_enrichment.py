import pytest

from tests.test_resources.helper import get_search, check_number_of_ads


@pytest.mark.parametrize("query, expected", [
    ('obstetrix', 40),
    ('tracheostomi', 62),
    ('yrkesbevis', 469),
    ('körkort', 22713),
    ('livsmedel', 512),
    ('livsmedel distributör', 38),
    (' distributör', 116),
])
def test_enrichment_skill( query, expected):
    json_response = get_search({'q': query})
    check_number_of_ads(json_response, expected)


@pytest.mark.parametrize("query, expected", [
    ('sköterska', 22),
    ('omsorgspersonal', 26),
    ('undersköterska', 2152),
    ('undersköterska omsorgspersonal', 2169),
    ('brandman', 36),
])
def test_enrichment_occupation( query, expected):
    check_number_of_ads(get_search({'q': query}), expected)


@pytest.mark.parametrize("query, expected", [
    ('lärarutbildning', 207),
    ('datasystem', 280),
    ('undervisning', 2635),
    ('its', 36),
    ('introduktionsprogram', 249),
    ('försäkring', 533),
    ('specialistutbildning', 789),
])
def test_enrichment_skills( query, expected):
    check_number_of_ads(get_search({'q': query}), expected)
