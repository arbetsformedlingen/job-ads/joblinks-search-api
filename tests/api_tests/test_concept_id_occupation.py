import pytest

from tests.test_resources.helper import get_search, check_number_of_ads, negative
from tests.test_resources.concept_ids import occupation_field as field
from tests.test_resources.concept_ids import occupation_group as group


@pytest.mark.parametrize("occupation, expected", [
    (group.barnmorskor, 75),
    (group.tandskoterskor, 184),
    (group.kockar_och_kallskankor, 2029),
    (group.grundutbildade_sjukskoterskor, 7998),
    ([group.mjukvaru__och_systemutvecklare_m_fl_, group.banktjansteman], 11827),
    ([group.mjukvaru__och_systemutvecklare_m_fl_, negative(group.banktjansteman)], 11825),
    (group.mjukvaru__och_systemutvecklare_m_fl_, 11825),

])
def test_occupation_group( occupation, expected):
    check_number_of_ads(get_search(params={'occupation-group': occupation}), expected)


@pytest.mark.parametrize("occupation, expected", [
    (field.naturbruk, 245),
    (field.hotell__restaurang__storhushall, 6330),
    (field.transport, 5810),
    (field.kultur__media__design, 202),
    ([field.administration__ekonomi__juridik, field.data_it], 25269),
    ([field.administration__ekonomi__juridik, negative(field.data_it)], 11089),
    (field.administration__ekonomi__juridik, 11089),
])
def test_occupation_field( occupation, expected):
    check_number_of_ads(get_search(params={'occupation-field': occupation}), expected)
