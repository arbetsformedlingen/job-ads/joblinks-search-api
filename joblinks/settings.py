import os

API_VERSION = '1.5.0'

# Elasticsearch settings
ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 9200)
ES_USER = os.getenv("ES_USER")
ES_PWD = os.getenv("ES_PWD")
ES_INDEX = os.getenv("ES_INDEX", "joblinks")

JAE_API_URL = os.getenv("JAE_API_URL", "https://jobad-enrichments-api.jobtechdev.se")

DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP = os.getenv('DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP', 'false').lower() == 'true'

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = False
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

# Query parameters
OCCUPATION = 'occupation-name'
GROUP = 'occupation-group'
FIELD = 'occupation-field'
MUNICIPALITY = 'municipality'
REGION = 'region'
COUNTRY = 'country'
QUERY = 'q'
OFFSET = 'offset'
LIMIT = 'limit'
FREETEXT_QUERY = 'q'
EXCLUDE_SOURCE = 'exclude_source'

FREETEXT_FIELDS = 'qfields'
SORT = 'sort'
# PUBLISHED_BEFORE = 'published-before'
PUBLISHED_AFTER = 'published-after'

DEFAULT_FREETEXT_BOOL_METHOD = 'or'

MAX_OFFSET = 2000
MAX_LIMIT = 100
