import re
from loguru import logger as log

from joblinks import settings
from joblinks.repository import fields as query_fields

QUERYFIELD_CHOICES = ["occupation", "skill", "location", "employer"]


class QueryBuilderFreetext(object):
    def __init__(self, text_to_concept):
        self.text_to_concept = text_to_concept

    def build_freetext_query(self, querystring, queryfields):
        if not querystring:
            return None

        if not queryfields:
            queryfields = QUERYFIELD_CHOICES.copy()

        querystring = self._remove_unwanted_chars_from_querystring(querystring)
        original_querystring = querystring

        (phrases, querystring) = self.extract_quoted_phrases(querystring)
        concepts = self.text_to_concept.text_to_concepts(querystring)
        querystring = self._rewrite_querystring(querystring, concepts)
        freetext_query = self._create_base_freetext_query(querystring)

        # Make all "musts" concepts "shoulds" as well
        for qf in queryfields:
            if qf in concepts:
                must_key = "%s_must" % qf
                concepts[qf] += [c for c in concepts.get(must_key, [])]

        for concept_type in queryfields:
            sub_should = self._freetext_concepts(
                {"bool": {}}, concepts, [concept_type], "should"
            )
            if "should" in sub_should["bool"]:
                if "must" not in freetext_query["bool"]:
                    freetext_query["bool"]["must"] = []
                freetext_query["bool"]["must"].append(sub_should)

        self._freetext_concepts(freetext_query, concepts, queryfields, "must_not")
        self._freetext_concepts(freetext_query, concepts, queryfields, "must")
        self._add_phrases_query(freetext_query, phrases)

        freetext_query = self._freetext_headline(freetext_query, original_querystring)

        return freetext_query

    def _remove_unwanted_chars_from_querystring(self, querystring: str) -> str:
        return " ".join([w.strip(",.!?:; ") for w in re.split("\\s|\\,", querystring)])

    def extract_quoted_phrases(self, text: str) -> dict:
        # Append quote to end of string if unbalanced
        if text.count('"') % 2 != 0:
            text += '"'

        must_matches = re.findall(r"\+\"(.+?)\"", text)
        mustnot_matches = re.findall(r"\-\"(.+?)\"", text)

        for notmust_match in mustnot_matches:
            text = re.sub('-"%s"' % notmust_match, "", text)

        for must_match in must_matches:
            text = re.sub(r'\+"%s"' % must_match, "", text)

        matches = re.findall(r"\"(.+?)\"", text)

        for match in matches:
            text = re.sub('"%s"' % match, "", text)

        return {
            "phrases": matches,
            "phrases_must": must_matches,
            "phrases_must_not": mustnot_matches,
        }, text.strip()

    def _rewrite_querystring(self, querystring, concepts):
        # Sort all concepts by string length
        all_concepts = sorted(
            concepts.get("occupation", [])
            + concepts.get("occupation_must", [])
            + concepts.get("occupation_must_not", [])
            + concepts.get("skill", [])
            + concepts.get("skill_must", [])
            + concepts.get("skill_must_not", [])
            + concepts.get("location", [])
            + concepts.get("location_must", [])
            + concepts.get("location_must_not", []),
            key=lambda c: len(c),
            reverse=True,
        )

        # Remove found concepts from querystring
        for term in [concept["term"] for concept in all_concepts]:
            term = self._rewrite_word_for_regex(term)
            p = re.compile(f"(^|\\s+)(\\+{term}|\\-{term}|{term})(\\s+|$)")
            querystring = p.sub("\\1\\3", querystring).strip()

        # Remove duplicate spaces
        querystring = re.sub("\\s+", " ", querystring).strip()

        return querystring

    def _create_base_freetext_query(self, querystring):
        # Creates a base query dict for "independent" freetext words
        # (e.g. words not found in text_to_concepts)
        method = "or"

        suffix_words = " ".join(
            [word[1:] for word in querystring.split(" ") if word.startswith("*")]
        )

        prefix_words = " ".join(
            [
                word[:-1]
                for word in querystring.split(" ")
                if word and word.endswith("*")
            ]
        )

        inc_words = " ".join(
            [
                word
                for word in querystring.split(" ")
                if word
                and not word.startswith("+")
                and not word.startswith("-")
                and not word.startswith("*")
                and not word.endswith("*")
            ]
        )

        must_words = " ".join(
            [
                word[1:]
                for word in querystring.split(" ")
                if word.startswith("+") and word[1:].strip()
            ]
        )

        exc_words = " ".join(
            [
                word[1:]
                for word in querystring.split(" ")
                if word.startswith("-") and word[1:].strip()
            ]
        )

        shoulds = self._freetext_fields(inc_words, method) if inc_words else []
        musts = self._freetext_fields(must_words, method) if must_words else []
        mustnots = self._freetext_fields(exc_words, method) if exc_words else []

        # If a language is a word in the search string.
        # Boost ads with the language found. Currently supports english.
        language = self._detect_freetext_language(inc_words)
        if language:
            shoulds.append(self._freetext_boost_ads_with_language(language))

        freetext_query = {"bool": {}}
        # Add "common" words to query
        if shoulds or musts or prefix_words or suffix_words:
            freetext_query["bool"]["must"] = []

        if shoulds:
            # Include all "must" words in should, to make sure any single "should"-word
            # not becomes exclusive
            if "must" not in freetext_query["bool"]:
                freetext_query["bool"]["must"] = []

            freetext_query["bool"]["must"].append({"bool": {"should": shoulds + musts}})

        # Wildcards after shoulds so they dont end up there
        if prefix_words:
            musts.append(self._freetext_wildcard(prefix_words, "prefix", method))

        if suffix_words:
            musts.append(self._freetext_wildcard(suffix_words, "suffix", method))

        if musts:
            freetext_query["bool"]["must"].append({"bool": {"must": musts}})

        if mustnots:
            freetext_query["bool"]["must_not"] = mustnots

        return freetext_query

    def _freetext_fields(
        self, searchword, method=settings.DEFAULT_FREETEXT_BOOL_METHOD
    ):
        return [
            {
                "multi_match": {
                    "query": searchword,
                    "type": "cross_fields",
                    "operator": method,
                    "fields": [
                        "originalJobPosting.title" + "^3",
                        query_fields.KEYWORDS_EXTRACTED + ".location^5",
                    ],
                }
            }
        ]

    def _detect_freetext_language(self, search_string):
        # Currently only supports english
        language = None
        if "english" in search_string:
            language = "en"

        return language

    def _freetext_concepts(self, query_dict, concepts, concept_keys, bool_type):
        for concept_key in concept_keys:
            dict_key = (
                "%s_%s" % (concept_key, bool_type)
                if bool_type != "should"
                else concept_key
            )
            current_concepts = [c for c in concepts.get(dict_key, []) if c]

            for concept in current_concepts:
                if bool_type not in query_dict["bool"]:
                    query_dict["bool"][bool_type] = []

                base_fields = []
                if concept_key == "location" and bool_type != "must":
                    base_fields.append(query_fields.KEYWORDS_EXTRACTED)
                    base_fields.append(query_fields.KEYWORDS_ENRICHED)
                    value = concept["term"].lower()
                    if value not in self.text_to_concept.get_extracted_ads_locations():
                        # Note: If a location is missing in the extracted_ads_locations (probably a smaller village),
                        # we should search in the ad title too to avoid that the ad won't show up in the result.
                        geo_ft_query = self._freetext_fields(value)
                        query_dict["bool"][bool_type].append(geo_ft_query[0])
                elif concept_key == "occupation" and bool_type != "must":
                    base_fields.append(query_fields.KEYWORDS_EXTRACTED)
                    base_fields.append(query_fields.KEYWORDS_ENRICHED)
                else:
                    curr_base_field = (
                        query_fields.KEYWORDS_EXTRACTED
                        if concept_key in ["employer"]
                        else query_fields.KEYWORDS_ENRICHED
                    )
                    base_fields.append(curr_base_field)

                for base_field in base_fields:
                    if base_field == query_fields.KEYWORDS_EXTRACTED:
                        value = concept["term"].lower()
                        boost_value = 10
                    else:
                        value = concept["concept"].lower()
                        boost_value = 9

                    field = "%s.%s.raw" % (base_field, concept_key)
                    query_dict["bool"][bool_type].append(
                        {"term": {field: {"value": value, "boost": boost_value}}}
                    )

        return query_dict

    def _add_phrases_query(self, ft_query, phrases):
        for bool_type in ["should", "must", "must_not"]:
            key = "phrases" if bool_type == "should" else "phrases_%s" % bool_type

            for phrase in phrases[key]:
                if bool_type not in ft_query["bool"]:
                    ft_query["bool"][bool_type] = []

                ft_query["bool"][bool_type].append(
                    {
                        "multi_match": {
                            "query": phrase,
                            "fields": [
                                "originalJobPosting.title",
                            ],
                            "type": "phrase",
                        }
                    }
                )

        return ft_query

    def _freetext_headline(self, query_dict, querystring):
        querystring = re.sub(r"(^| )[\\+]{1}", " ", querystring)
        querystring = " ".join(
            [
                word
                for word in querystring.split(" ")
                if not word.startswith("-")
                and not word.startswith("*")
                and not word.endswith("*")
            ]
        )

        if "must" not in query_dict["bool"]:
            query_dict["bool"]["must"] = []

        for should in query_dict["bool"]["must"]:
            try:
                should["bool"]["should"].append(
                    {
                        "match": {
                            "originalJobPosting.title"
                            + ".words": {
                                "query": querystring.strip(),
                                "operator": "and",
                                "boost": 5,
                            }
                        }
                    }
                )
            except KeyError:
                log.error("No bool clause for title query")

        return query_dict

    def _rewrite_word_for_regex(self, word: str) -> str:
        """Inserts a \ in front of the characters in bad_chards"""
        if word is None:
            word = ""

        bad_chars = [
            "+",
            ".",
            "[",
            "]",
            "{",
            "}",
            "(",
            ")",
            "^",
            "$",
            "*",
            "\\",
            "|",
            "?",
            '"',
            "'",
            "&",
            "<",
            ">",
        ]

        if any(c in bad_chars for c in word):
            modded_term = ""
            for c in word:
                if c in bad_chars:
                    modded_term += "\\"
                modded_term += c
            return modded_term

        return word

    def _freetext_wildcard(
        self, searchword, wildcard_side, method=settings.DEFAULT_FREETEXT_BOOL_METHOD
    ):
        return {
            "multi_match": {
                "query": searchword,
                "type": "cross_fields",
                "operator": method,
                "fields": [
                    "originalJobPosting.title" + "." + wildcard_side,
                ],
            }
        }

    def _freetext_boost_ads_with_language(self, language):
        return {"term": {"detected_language.keyword": {"value": language, "boost": 10}}}
