from joblinks.repository import fields as query_fields


class QueryBuilderLocation(object):

    # Parses MUNICIPALITY and REGION
    def build_location_query(self, municipality_codes, region_codes, country_codes):
        municipalities = []
        mustnot_municipalities = []
        regions = []
        mustnot_regions = []
        country = []
        mustnot_country = []

        for mpty_code in municipality_codes if municipality_codes else []:
            if mpty_code.startswith("-"):
                mustnot_municipalities.append(mpty_code[1:])
            else:
                municipalities.append(mpty_code)

        for region_code in region_codes if region_codes else []:
            if region_code.startswith("-"):
                mustnot_regions.append(region_code[1:])
            else:
                regions.append(region_code)

        for country_code in country_codes if country_codes else []:
            if country_code.startswith("-"):
                mustnot_country.append(country_code[1:])
            else:
                country.append(country_code)

        # ------------------------------------------------

        should_location_term_query = [
            {
                "term": {
                    query_fields.WORKPLACE_ADDRESS_MUNICIPALITY_CONCEPT_ID: {
                        "value": mpty_code,
                        "boost": 2.0,
                    }
                }
            }
            for mpty_code in municipalities
        ]

        should_location_term_query += [
            {
                "term": {
                    query_fields.WORKPLACE_ADDRESS_REGION_CONCEPT_ID: {
                        "value": region_code,
                        "boost": 1.0,
                    }
                }
            }
            for region_code in regions
        ]

        should_location_term_query += [
            {
                "term": {
                    query_fields.WORKPLACE_ADDRESS_COUNTRY_CONCEPT_ID: {
                        "value": country_code,
                        "boost": 1.0,
                    }
                }
            }
            for country_code in country
        ]

        # ------------------------------------------
        mustnot_municipality_term_query = []
        mustnot_region_term_query = []
        mustnot_country_term_query = []

        if mustnot_municipalities:
            mustnot_municipality_term_query += [
                {
                    "term": {
                        query_fields.WORKPLACE_ADDRESS_MUNICIPALITY_CONCEPT_ID: {
                            "value": mpty_code
                        }
                    }
                }
                for mpty_code in mustnot_municipalities
            ]

        if mustnot_regions:
            mustnot_region_term_query += [
                {
                    "term": {
                        query_fields.WORKPLACE_ADDRESS_REGION_CONCEPT_ID: {
                            "value": region_code
                        }
                    }
                }
                for region_code in mustnot_regions
            ]

        if mustnot_country:
            mustnot_country_term_query += [
                {
                    "term": {
                        query_fields.WORKPLACE_ADDRESS_COUNTRY_CONCEPT_ID: {
                            "value": contry_code
                        }
                    }
                }
                for contry_code in mustnot_country
            ]

        # ---------------------------------------------
        if (
            should_location_term_query
            or mustnot_municipality_term_query
            or mustnot_region_term_query
            or mustnot_country_term_query
        ):
            return {
                "nested": {
                    "path": "workplace_addresses",
                    "query": {
                        "bool": {
                            "should": should_location_term_query,
                            "must_not": mustnot_municipality_term_query
                            + mustnot_region_term_query
                            + mustnot_country_term_query,
                        }
                    },
                }
            }
        else:
            return None
